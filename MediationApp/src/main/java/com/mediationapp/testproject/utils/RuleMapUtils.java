package com.mediationapp.testproject.utils;

import com.mediationapp.testproject.entity.Answer;
import com.mediationapp.testproject.entity.Question;
import com.mediationapp.testproject.entity.QuestionnaireItem;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


public class RuleMapUtils{


    private static final Logger LOGGER = LogManager.getLogger(RuleMapUtils.class.getName());

    public static Map<QuestionnaireItem, String> generateRuleMap(int questions, int answers) {
        Map<QuestionnaireItem, String> scriptItemsMap = new ConcurrentHashMap<>();
        for (int i = 0; i < questions; i++) {
            Question q = new Question("Question " + i);
            for (int j = 0; j < answers; j++) {
                Answer a = new Answer("Answer " + j + " for Question " + i);
                scriptItemsMap.put(new QuestionnaireItem(q, a), "Script for Answer " + j + " for Question " + i);
            }
        }
        LOGGER.trace("generated map of "+scriptItemsMap.size());
        return scriptItemsMap;
    }
}
