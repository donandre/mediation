package com.mediationapp.testproject.utils;

import com.mediationapp.testproject.entity.Answer;
import com.mediationapp.testproject.entity.Question;
import com.mediationapp.testproject.entity.QuestionnaireItem;

import java.util.LinkedHashSet;
import java.util.Set;


public class QuestionnaireItemsUtils {

    public static Set<QuestionnaireItem> generateQuestionnaireItems(int questions, int answers) {
        Set<QuestionnaireItem> questionnaireItemsSet = new LinkedHashSet<>();
        for (int i = 0; i < questions; i++) {
            Question q = new Question("Question " + i);
            for (int j = 0; j < answers; j++) {
                Answer a = new Answer("Answer " + j + " for Question " + i);
                questionnaireItemsSet.add(new QuestionnaireItem(q, a));
            }
        }
        return questionnaireItemsSet;
    }
}
