package com.mediationapp.testproject.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MediationAppController {
    @RequestMapping("/hello")
    public String outHello(@RequestParam(value = "name") String name) {
        return "Hello " + name;
    }
}
