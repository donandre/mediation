package com.mediationapp.testproject.controller;

import com.mediationapp.testproject.entity.QuestionnaireItem;
import com.mediationapp.testproject.service.QuestionnaireRuleService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping(value = "/questions")
public class QuestionnaireController {
    private static final Logger LOGGER = LogManager.getLogger(QuestionnaireController.class.getName());

    private QuestionnaireRuleService questionnaireRuleService;

    @RequestMapping(value = "/All", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    @ResponseBody
    public Map<QuestionnaireItem, String> getAnsweredQuestions(){
        LOGGER.info("entering getAllQuestions");
        return questionnaireRuleService.getRules();
    }
    @RequestMapping("/evict")
    public String evictCache(){
        questionnaireRuleService.evictRuleCache();
        return "Cache eviction called";
    }
    @Autowired
    public void setQuestionnaireRuleService(QuestionnaireRuleService questionnaireRuleService) {
        this.questionnaireRuleService = questionnaireRuleService;
    }
}
