package com.mediationapp.testproject.controller;

import com.mediationapp.testproject.service.ScriptService;
import com.mediationapp.testproject.entity.Script;
import com.mediationapp.testproject.repository.ScriptRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/script")
public class ScriptController {
    private static final Logger LOGGER = LogManager.getLogger(ScriptController.class.getName());

    private ScriptRepository scriptRepository;
    private ScriptService scriptService;

    @RequestMapping(value = "/All", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    @ResponseBody
    public List<Script> getAllScripts() {
        return scriptRepository.findAll();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    @ResponseBody
    public Optional<Script> getScriptById(@PathVariable Long id) {
//handle null values
        LOGGER.info("handling findById method");
        return scriptRepository.findById(id);
    }

    @RequestMapping(value = "/count", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    @ResponseBody
    public int getScriptsCount() {
        return scriptRepository.getCount();
    }

    @RequestMapping(value = "/populate/{count}")
    public String populateScripts(@PathVariable Long count) {
        return scriptService.populateScripts(count);
    }

    @Autowired
    public void setScriptRepository(ScriptRepository scriptRepository) {
        this.scriptRepository = scriptRepository;
    }

    @Autowired
    public void setScriptService(ScriptService scriptService) {
        this.scriptService = scriptService;
    }
}
