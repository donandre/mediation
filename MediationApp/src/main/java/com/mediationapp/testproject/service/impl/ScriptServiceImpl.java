package com.mediationapp.testproject.service.impl;

import com.mediationapp.testproject.entity.Script;
import com.mediationapp.testproject.repository.ScriptRepository;
import com.mediationapp.testproject.service.ScriptService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Random;

@Service
public class ScriptServiceImpl implements ScriptService {
    private ScriptRepository scriptRepository;


    private static final Logger LOGGER = LogManager.getLogger(ScriptServiceImpl.class.getName());

    @Override
    public String populateScripts(Long qnt) {
        String dummyBody = "SELECT NULL AS \"Dummy column\";";
        for (Long i = 0L; i < qnt; i++) {
            Script script = new Script();
            script.setBody(dummyBody);
            script.setFilename("dummyFile" + i + ".sql");
            scriptRepository.save(script);
        }
        return "done";
    }

    public Optional<Script> getRandomScriptFromRepository() {
        long range = 8L;
        Random r = new Random();
        long retrieval = (long) (r.nextDouble() * range) + 1L;
        return scriptRepository.findAll().stream()
                .filter(s -> s.getId().equals(retrieval))
                .findFirst();
    }


    @Autowired
    public void setScriptRepository(ScriptRepository scriptRepository) {
        this.scriptRepository = scriptRepository;
    }
}
