package com.mediationapp.testproject.service;

import com.mediationapp.testproject.entity.QuestionnaireItem;

import java.util.Map;

public interface QuestionnaireRuleService {
    Map<QuestionnaireItem, String> getRules();
    void evictRuleCache();
}
