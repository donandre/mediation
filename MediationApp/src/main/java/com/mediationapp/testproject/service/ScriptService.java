package com.mediationapp.testproject.service;

import com.mediationapp.testproject.entity.Script;

import java.util.Optional;

public interface ScriptService {
    String populateScripts(Long qnt);
    Optional<Script> getRandomScriptFromRepository();
}
