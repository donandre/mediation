package com.mediationapp.testproject.service.impl;

import com.mediationapp.testproject.entity.QuestionnaireItem;
import com.mediationapp.testproject.repository.RuleRepository;
import com.mediationapp.testproject.repository.impl.RuleRepositoryImpl;
import com.mediationapp.testproject.service.QuestionnaireRuleService;
import com.mediationapp.testproject.utils.QuestionnaireItemsUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Service
public class QuestionnaireRuleServiceImpl implements QuestionnaireRuleService {
    private RuleRepository ruleRepository;

    private static final Logger LOGGER = LogManager.getLogger(RuleRepositoryImpl.class.getName());

    @Override
    public Map<QuestionnaireItem, String> getRules() {
        //getting set of user's answered questions
        Set<QuestionnaireItem> userQuestionnaireItemSet = QuestionnaireItemsUtils.generateQuestionnaireItems(100, 1);

        Map<QuestionnaireItem, String> output = new HashMap<>();
        for (QuestionnaireItem q : userQuestionnaireItemSet) {
            output.put(q, ruleRepository.getRuleMap().get(q));
        }
        return output;
    }

    @CacheEvict(value = "ruleMapCache", allEntries = true)
    public void evictRuleCache() {
        LOGGER.info("Evicting all entries from ruleMapCache");
    }

    @Autowired
    public void setRuleRepository(RuleRepository ruleRepository) {
        this.ruleRepository = ruleRepository;
    }
}
