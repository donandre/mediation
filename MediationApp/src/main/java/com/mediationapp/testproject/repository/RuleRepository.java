package com.mediationapp.testproject.repository;

import com.mediationapp.testproject.entity.QuestionnaireItem;

import java.util.Map;

public interface RuleRepository {
    Map<QuestionnaireItem, String> getRuleMap();
}
