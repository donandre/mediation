package com.mediationapp.testproject.repository;

import com.mediationapp.testproject.entity.Script;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ScriptRepository extends JpaRepository<Script, Long> {
    @Cacheable(value="AllScriptsCache")
    List<Script> findAll();
    Script findByFilename(String filename);
    @Query(nativeQuery = true, value ="SELECT count(id) from mediation.script")
    int getCount();
}
