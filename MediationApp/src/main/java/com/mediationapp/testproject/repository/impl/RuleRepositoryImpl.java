package com.mediationapp.testproject.repository.impl;

import com.mediationapp.testproject.entity.QuestionnaireItem;
import com.mediationapp.testproject.repository.RuleRepository;
import com.mediationapp.testproject.utils.RuleMapUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
public class RuleRepositoryImpl implements RuleRepository {


    private static final Logger LOGGER = LogManager.getLogger(RuleRepositoryImpl.class.getName());

    @Override
    @Cacheable("ruleMapCache")
    public Map<QuestionnaireItem, String> getRuleMap() {
        Map<QuestionnaireItem, String> ruleMap = RuleMapUtils.generateRuleMap(8000, 10);
        LOGGER.warn("Re-generated ruleMap of " + ruleMap.size());
        return ruleMap;
    }


}
