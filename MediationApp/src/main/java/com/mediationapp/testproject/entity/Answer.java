package com.mediationapp.testproject.entity;

import java.util.Objects;

public final class Answer {
    private String body;

    public String getBody() {
        return body;
    }

    public Answer(String body) {
        this.body = body;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Answer answer = (Answer) o;
        return body.equals(answer.body);
    }

    @Override
    public int hashCode() {
        return Objects.hash(body);
    }

    @Override
    public String toString() {
        return "Answer{" +
                "body='" + body + '\'' +
                '}';
    }
}
