package com.mediationapp.testproject.entity;

import javax.persistence.*;

@Entity
@Table(name = "script")
public class Script {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    @Column(name = "filename")
    private String filename;
    @Column(name = "body")
    private String body;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "Script{" +
                "id=" + id +
                ", filename='" + filename + '\'' +
                ", body='" + body + '\'' +
                '}';
    }
}
