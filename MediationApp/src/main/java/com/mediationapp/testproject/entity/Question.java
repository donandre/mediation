package com.mediationapp.testproject.entity;

import java.util.Objects;

public final class Question {
    private final String body;

    public String getBody() {
        return body;
    }

    public Question(String body) {
        this.body = body;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Question question = (Question) o;
        return body.equals(question.body);
    }

    @Override
    public int hashCode() {
        return Objects.hash(body);
    }

    @Override
    public String toString() {
        return "Question{" +
                "body='" + body + '\'' +
                '}';
    }
}
