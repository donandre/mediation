package com.mediationapp.testproject.entity;

import java.util.Objects;

public final class QuestionnaireItem {
    private Question question;
    private Answer answer;

    public Question getQuestion() {
        return question;
    }

    public Answer getAnswer() {
        return answer;
    }

    public QuestionnaireItem(Question question, Answer answer) {
        this.question = question;
        this.answer = answer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        QuestionnaireItem that = (QuestionnaireItem) o;
        return question.equals(that.question) &&
                answer.equals(that.answer);
    }

    @Override
    public int hashCode() {
        return Objects.hash(question, answer);
    }

    @Override
    public String toString() {
        return "QuestionnaireItem{" +
                "question=" + question +
                ", answer=" + answer +
                '}';
    }
}
