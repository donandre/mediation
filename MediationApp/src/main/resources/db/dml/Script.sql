insert into mediation.script (id, filename, body) values (1, 'select.sql', 'SELECT * FROM mediation.SCRIPT;');
insert into mediation.script (id, filename, body) values (2, 'execute_function.sql','select mediation.dummy_function(6);');
insert into mediation.script (id, filename, body) values (3, 'execute_block.sql', 'DO $$
DECLARE
  counter integer := 0;
BEGIN
   counter := counter + 1;
   RAISE NOTICE ''The current value of counter is %'', counter;
END$$;');