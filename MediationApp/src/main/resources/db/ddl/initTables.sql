CREATE TABLE mediation.Script (
    id bigserial primary key,
    filename varchar,
    body varchar
);