CREATE FUNCTION mediation.dummy_function(id int) RETURNS void AS $$
    #variable_conflict use_variable
    DECLARE

    BEGIN
	RAISE NOTICE 'dummy_function called';
    END;
$$ LANGUAGE plpgsql;